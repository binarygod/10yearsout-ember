﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using BrainInABox.Models;
using BrainInABox.Entities;
using BrainInABox.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace BrainInABox.Controllers
{
    [Produces("application/vnd.api+json")]
    [Route("api/v1/token")]
    public class TokenController : Controller
    {
        private IConfiguration _config;
        private ApplicationDBContext _db;

        public TokenController(IConfiguration config, ApplicationDBContext db)
        {
            _config = config;
            _db = db;
        }

        /// <summary>
        /// Requests JWT Token
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Todo
        ///     {
        ///        "id": 1,
        ///        "name": "Item1",
        ///        "isComplete": true
        ///     }
        ///
        /// </remarks>
        /// <param name="login">Username and Password to authenticate</param>
        /// <returns>Signed JWT Token</returns>
        /// <response code="200">Returns the signed JWT Token</response>
        /// <response code="400">If username or password is incorrect</response>     
        [HttpPost, AllowAnonymous]
        [ProducesResponseType(typeof(TokenModel), 200)]
        [ProducesResponseType(typeof(string), 400)]
        public IActionResult Post([FromBody] AuthenticationModel login)
        {
            IActionResult response = Unauthorized();
            User user = _db.Users.SingleOrDefault(e => e.Username == login.Username);

            if(user != null)
            {
                if(user.Password == login.Password)
                {
                    List<Claim> claims = new List<Claim>()
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                        new Claim(JwtRegisteredClaimNames.Email, user.Email)
                    };

                    foreach(string role in user.GetRoles())
                    {
                        claims.Add(new Claim(ClaimTypes.Role, role));
                    }

                    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                    SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    JwtSecurityToken token = new JwtSecurityToken(_config["Jwt:Issuer"], null, claims, expires: DateTime.Now.AddMinutes(30), signingCredentials: creds);

                    //response = Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
                    TokenModel tModel = new TokenModel();
                    tModel.Token = new JwtSecurityTokenHandler().WriteToken(token);
                    tModel.Email = user.Email;
                    return Ok(tModel);
                }
                else
                {
                    response = StatusCode(StatusCodes.Status400BadRequest, "Username or Password incorrect");
                }
            } else
            {
                response = StatusCode(StatusCodes.Status400BadRequest, "Username or Password incorrect");
            }

            return response;
        }
    }
}
