﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BrainInABox.Entities;
using BrainInABox.Data;
using JsonApiSerializer;
using Microsoft.AspNetCore.Authorization;
using BrainInABox.Helpers;

namespace BrainInABox.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/users")]
    public class UsersController : Controller
    {
        ApplicationDBContext _db;

        public UsersController(ApplicationDBContext db)
        {
            _db = db;
        }

        // GET: api/Users
        /// <summary>
        /// Lists all Users
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <returns>List of Users</returns>
        /// <response code="200">
        /// 
        /// Sample Response:
        /// 
        ///     {
        ///         "data": [
        ///             {
        ///                 "type": "user",
        ///                 "id": 1,
        ///                 "attributes": {
        ///                     "username": "username1",
        ///                     "email": "user1@domain.com",
        ///                     "password": "P@55w0rd",
        ///                     "isActive": true,
        ///                     "roles": "ROLE_USER,ROLE_EMPLOYEE,ROLE_GOD,ROLE_ADMIN"
        ///                 }
        ///             },
        ///             {
        ///                 "type": "user",
        ///                 "id": 2,
        ///                 "attributes": {
        ///                     "username": "username2",
        ///                     "email": "user2@domain.com",
        ///                     "password": "P@55w0rd",
        ///                     "isActive": true,
        ///                     "roles": "ROLE_USER,ROLE_EMPLOYEE"
        ///                 }
        ///             }
        ///         ]
        ///     }
        ///     
        /// </response>
        [HttpGet, Authorize(Roles = "ROLE_ADMIN")]
        public IActionResult Get()
        {          
            return new JsonResult(_db.Users.ToList(), new JsonApiSerializerSettings());
        }

        // GET: api/Users/5
        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="id">User Record Id</param>
        /// <returns>User</returns>
        /// <response code="200">
        /// 
        /// Sample Response:
        /// 
        ///     {
        ///         "data": {
        ///             "type": "user",
        ///             "id": 1,
        ///             "attributes": {
        ///                 "username": "username1",
        ///                 "email": "user1@domain.com",
        ///                 "password": "P@55w0rd",
        ///                 "isActive": true,
        ///                 "roles": "ROLE_USER,ROLE_EMPLOYEE,ROLE_GOD,ROLE_ADMIN"
        ///             }
        ///         }
        ///     }
        ///     
        /// </response>
        /// <response code="400">
        /// 
        /// Sample Response:
        /// 
        ///     {
        ///         "errors": {
        ///             "details": "User doesn't exist",
        ///             "source": {
        ///                 "pointer": "data/id"
        ///             }
        ///         }
        ///     }
        /// 
        /// </response>
        [HttpGet("{id}", Name = "Get"), Authorize(Roles = "ROLE_ADMIN")]
        public IActionResult Get(int id)
        {
            try
            {

                return new JsonResult(_db.Users.Single(u => u.Id == id), new JsonApiSerializerSettings());
            } catch(Exception ex)
            {
                return ErrorHelper.Error("User doesn't exist", "data/id");
            }
        }

        // POST: api/Users
        /// <summary>
        /// Create a User
        /// </summary>
        /// <param name="user">User to Create</param>
        /// <response code="200">User created successfully</response>
        /// <response code="400">Bad Request
        /// 
        /// Sample Response:
        /// 
        /// {
        ///     "errors": {
        ///         "details": "Email already exists",
        ///         "source": {
        ///             "pointer": "data/attributes/email"
        ///         }
        ///     }
        /// }
        /// 
        /// </response>
        [HttpPost]
        public IActionResult Post([FromBody]User user)
        {
            IActionResult response = Unauthorized();

            if(_db.Users.SingleOrDefault(u => u.Username == user.Username) == null)
            {
                if(_db.Users.SingleOrDefault(u => u.Email == user.Email) == null)
                {

                    _db.Users.Add(user);
                    _db.SaveChanges();

                    response = Ok("Added new user");
                } else
                {
                    response = BadRequest("Email already exists");
                }
            } else
            {
                response = BadRequest("Username already exists");
            }

            return response;
        }

        // PUT: api/Users/5
        /// <summary>
        /// Update a User
        /// </summary>
        /// <param name="id">User Record Id</param>
        /// <param name="user">User Object</param>
        /// <response code="200">User updated successfully</response>
        /// <response code="400">Bad Request
        /// 
        /// Sample Response:
        /// 
        /// {
        ///     "errors": {
        ///         "details": "Email already exists",
        ///         "source": {
        ///             "pointer": "data/attributes/email"
        ///         }
        ///     }
        /// }
        /// 
        /// </response>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]User user)
        {
            User dbUser = _db.Users.SingleOrDefault(u => u.Id == user.Id);

            if(dbUser != null)
            {
                dbUser.IsActive = user.IsActive;
                dbUser.Password = user.Password;
                dbUser.Roles = user.Roles;

                try
                {
                    _db.SaveChanges();

                    return Ok("Saved successfully");
                } catch(Exception ex)
                {
                    return ErrorHelper.Error("Error saving user", "data", ex.Message);
                }
            } else
            {
                return ErrorHelper.Error("User doesn't exist", "data/id");
            }
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// Deletes a User
        /// </summary>
        /// <param name="id">User Record Id</param>
        /// <response code="200">User deleted successfully</response>
        /// <response code="400">Bad Request
        /// 
        /// Sample Response:
        /// 
        /// {
        ///     "errors": {
        ///         "details": "Email already exists",
        ///         "source": {
        ///             "pointer": "data/attributes/email"
        ///         }
        ///     }
        /// }
        /// 
        /// </response>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            User user = _db.Users.SingleOrDefault(u => u.Id == id);

            if(user != null)
            {
                try
                {
                    _db.Users.Remove(user);
                    _db.SaveChanges();

                    return Ok("User deleted successfully");
                }catch(Exception ex)
                {
                    return ErrorHelper.Error("Error deleting user", "data/id", ex.Message);
                }

            } else
            {
                return ErrorHelper.Error("User doesn't exist", "data/id");
            }
        }
    }
}
