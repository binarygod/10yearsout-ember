﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BrainInABox.Entities;
using BrainInABox.Data;
using JsonApiSerializer;
using Microsoft.AspNetCore.Authorization;
using BrainInABox.Helpers;

namespace BrainInABox.Helpers
{
    public static class ErrorHelper
    {
        public static IActionResult Error(string detail, string pointer, string description = null)
        {
            Dictionary<string, object> results = new Dictionary<string, object>();

            Dictionary<string, object> errors = new Dictionary<string, object>();

            errors.Add("details", detail);

            if(description != null)
            {
                errors.Add("description", description);
            }

            Dictionary<string, object> pointers = new Dictionary<string, object>();

            pointers.Add("pointer", pointer);

            errors.Add("source", pointers);
            results.Add("errors", errors);

            return new BadRequestObjectResult(new JsonResult(results, new JsonApiSerializerSettings()).Value);
        }
    }
}
