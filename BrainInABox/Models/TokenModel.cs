﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrainInABox.Models
{
    public class TokenModel
    {
        public TokenModel(string token = "", string email = "")
        {
            this.Token = token;
            this.Email = email;
        }
        public string Token { get; set; }
        public string Email { get; set; }
        /*public int Id { get; set; }*/
    }
}
