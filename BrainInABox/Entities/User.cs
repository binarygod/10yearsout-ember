﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using JsonApiSerializer;
using Newtonsoft.Json;

namespace BrainInABox.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [JsonProperty(propertyName: "is-active")]
        public bool IsActive { get; set; }
        public string Roles { get; set; } = "";

        public List<string> GetRoles()
        {
            return this.Roles.Split(",").ToList();
        }

        public void AddRole(string role)
        {
            List<string> roles = this.GetRoles();

            roles.Add(role);

            this.Roles = String.Join(",", roles.ToArray()).Trim(',');
        }

        public void RemoveRole(string role)
        {
            List<string> roles = this.GetRoles();

            roles.Remove(role);

            this.Roles = String.Join(",", roles.ToArray()).Trim(',');
        }
    }
}
