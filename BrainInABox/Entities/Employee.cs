﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrainInABox.Entities
{
    public class Employee
    {
        public int Id { get; set; }
        public bool IsHourly { get; set; }
        public decimal Rate { get; set; }
    }
}
