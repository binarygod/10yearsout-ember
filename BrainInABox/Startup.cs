﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using BrainInABox.Data;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using System.IO;
using System;
using Swashbuckle.AspNetCore.Swagger;
using System.Buffers;
using Microsoft.Extensions.ObjectPool;
using JsonApiSerializer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Logging;


namespace BrainInABox
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sp = services.BuildServiceProvider();
            var logger = sp.GetService<ILoggerFactory>();
            var objectPoolProvider = sp.GetService<ObjectPoolProvider>();

            // Create ApplicationDBContext
            services.AddDbContext<ApplicationDBContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            // Make our Configuration Injectable
            services.AddSingleton<IConfiguration>(Configuration);

            // Setup JWT Authentication
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });

            // Setup Policy Roles (IE: Role Heirachy)
            services.AddAuthorization(options => 
                options.AddPolicy("GROUP_WRITERS", policy =>
                  policy.RequireRole("ROLE_ADM1IN", "ROLE_BOB", "ROLE_KATE")));
            
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins("http://10.0.1.93:4200")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials() );
            });
            
            services.AddMvc(opt => {
                var serializerSettings = new JsonApiSerializerSettings();

                var jsonApiFormatter = new JsonOutputFormatter(serializerSettings, ArrayPool<Char>.Shared);
                opt.OutputFormatters.RemoveType<JsonOutputFormatter>();
                opt.OutputFormatters.Insert(0, jsonApiFormatter);

                var jsonApiInputFormatter = new JsonInputFormatter(logger.CreateLogger<JsonInputFormatter>(), serializerSettings, ArrayPool<Char>.Shared, objectPoolProvider);
                opt.InputFormatters.RemoveType<JsonInputFormatter>();
                opt.InputFormatters.Insert(0, jsonApiInputFormatter);

            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "10 Years Out",
                    Description = "Codenamed 'Brain In A Box' 10 Years Out API provides endpoints for all internal CBC applications.",
                    TermsOfService = "None",
                    Contact = new Contact() { Name = "Tim Davis", Email = "tim@cbcohio.net", Url = "http://10yearsout.net" },
                    License = new License() { Name = "CBC Proprietary" }
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "BrainInABox.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseCors("CorsPolicy"); 
            //.WithOrigins("http://10.0.1.93:4200").AllowAnyHeader().AllowAnyMethod());
            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "10 Years Out V1");
            });
        }
    }
}
